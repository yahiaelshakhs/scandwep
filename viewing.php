
<!DOCTYPE html>

<html>
<title>scandiwep</title>
<body>
   <form name="delete-action" action="delete.php" method="POST" >
  <?php include('templates/header.php'); ?>
  <div class="container">
       <div class="container">
         <div class="row">
        <?php
        include('class/sql_classes.php');
        $select = new sqlorders ;
        $products =  $select->selectproduct();
          foreach($products as $product){
              if ( $product['typeid'] == "1" ){?>
                <!-- for each dvd product -->
                <div class="col-sm">
                    <!-- view dvd products in 1 column -->
                            <div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
                                <div class="card-header">
                                    <!-- set delete checkbox in header -->
                                      <!-- <set the check box> -->
                                    <div class="custom-control custom-switch">
                                            <div class="custom-control custom-switch">
                                                    <input type="checkbox"
                                                     class="custom-control-input"
                                                    name="record[]"
                                                     value="<?php echo $product['id']; ?>"
                                                    id="<?php echo $product['id']; ?>">
                                                    <label
                                                    class="custom-control-label"
                                                    for="<?php echo $product['id']; ?>">
                                                    check to delete</label>
                                                </div>
                                                <!-- <set a hidden button> -->
                                    </div>
                                </div>
                                <!-- print values of product here -->
                                <div class="card-body">
                                        <h5 class="card-title"><?php echo $product['pr_name']; ?></h5>
                                  <p class="card-text">
                                              <div>
                                                sku:
                                                <?php echo ($product['sku']); ?>
                                              </div>
                                              <div>
                                                price :
                                                <?php echo ($product['price']); ?>.$
                                              </div>
                                              <div>
                                                size :
                                                <?php echo ($product['property']);  ?>mb
                                              </div>
                                  </p>
                                </div>
                            </div>
                 </div>
               <?php }}?>
        </div>
    </div>
<!-- same goes on with book and furn just as dvd -->
    <div class="container">
    <div class="row">
      <?php foreach($products as $product){
        if ( $product['typeid'] == "2" ){ ?>
                    <div class="col-sm">
                        <div class="card text-white bg-success mb-3" style="max-width: 18rem;">
                                <div class="card-header">
                                                <div class="custom-control custom-switch">
                                                        <input type="checkbox"
                                                        class="custom-control-input"
                                                        name="record[]"
                                                        value="<?php echo $product['id'] ;?>"
                                                        id="<?php echo $product['id'] ;?>">
                                                        <label class="custom-control-label"
                                                        for="<?php echo $product['id'] ;?>" >
                                                        Check to delete </label>
                                                </div>
                                    </div>
                                <div class="card-body">
                                  <h5 class="card-title"><?php echo $product['pr_name']; ?></h5>
                                  <p class=
                                  <div>
                                        sku:
                                        <?php echo ($product['sku']); ?>
                                      </div>
                                      <div>
                                        price :
                                        <?php echo ($product['price']); ?>.$
                                      </div>
                                      <div>
                                        weight :
                                        <?php echo ($product['property']);  ?>.gm
                                      </div></p>
                                </div>
                    </div>
                  <?php }}?>
    </div>
    </div>
    <div class="container">
        <div class="row">
          <?php foreach($products as $product){
             if ( $product['typeid'] == "3" ){ ?>
                    <div class="col-sm">
                        <div class="card text-white bg-info mb-3" style="max-width: 18rem;">
                                <div class="card-header">
                                                <div class="custom-control custom-switch">
                                                        <input type="checkbox"
                                                        class="custom-control-input"
                                                        name="record[]"
                                                        value="<?php echo $product['id'];?>"
                                                        id="<?php echo $product['id']; ?>">
                                                        <label
                                                        class="custom-control-label"
                                                        for="<?php echo $product['id']; ?>" >check to delete</label>
                                                </div>
                                </div>
                                <div class="card-body">
                                  <h5 class="card-title"><?php echo $product['pr_name']; ?></h5>
                                  <p class="card-text">
                                       <div>
                                        sku:
                                        <?php echo $product['sku']; ?>
                                      </div>
                                      <div>
                                        price :
                                        <?php echo $product['price']; ?>.$
                                      </div>
                                      <div>
                                            Dimensions :
                                        <?php echo $product['property'] ?>
                                    </div>
                                   </p>
                                </div>
                        </div>
                    </div>
            <?php  } } ?>
        </div>
      </div>


    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="check all.js"></script>

  <?php include('templates/footer.php'); ?>
</form>

</body>
</html>
